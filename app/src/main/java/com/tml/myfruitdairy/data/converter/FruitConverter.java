package com.tml.myfruitdairy.data.converter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tml.myfruitdairy.data.model.FruitModel;

import java.util.ArrayList;
import java.util.List;

public class FruitConverter {

    @TypeConverter
    public String fromStringToFruits(ArrayList<FruitModel> value) {
        if (value == null) {
            return (null);
        }
        return new Gson().toJson(value, new TypeToken<ArrayList<FruitModel>>() {
        }.getType());
    }

    @TypeConverter
    public ArrayList<FruitModel> fromFruitsToString(String value) {
        if (value == null) {
            return (null);
        }
        return new Gson().fromJson(value, new TypeToken<List<FruitModel>>() {
        }.getType());
    }
}
