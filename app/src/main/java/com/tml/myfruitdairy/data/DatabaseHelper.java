package com.tml.myfruitdairy.data;

import android.content.Context;

import androidx.room.Room;

public class DatabaseHelper {
    private static DatabaseHelper instance;
    private AppDatabase db;

    private DatabaseHelper(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, "database").build();
    }

    public static DatabaseHelper init(Context context) {
        return instance = instance == null ? new DatabaseHelper(context) : instance;
    }

    public static AppDatabase getDatabase() {
        return instance.db;
    }
}
