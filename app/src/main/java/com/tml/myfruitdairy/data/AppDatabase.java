package com.tml.myfruitdairy.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.tml.myfruitdairy.data.converter.FruitConverter;
import com.tml.myfruitdairy.data.dbInterface.FruitInterface;
import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;

@Database(entities = {FruitModel.class, SaveFruitModel.class}, version = 1)
@TypeConverters({FruitConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract FruitInterface chatRoomInterface();
}