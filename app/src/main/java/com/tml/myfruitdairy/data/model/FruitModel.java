package com.tml.myfruitdairy.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FruitModel {
    @PrimaryKey
    @NonNull
    public int id;

    @ColumnInfo(name = "type")
    public String type;

    @ColumnInfo(name = "vitamins")
    public int vitamins;

    @ColumnInfo(name = "image")
    public String image;

    @ColumnInfo(name = "fruitType")
    public String fruitType;

    @ColumnInfo(name = "amount")
    public int amount;

    @ColumnInfo(name = "fruitId")
    public int fruitId;

}
