package com.tml.myfruitdairy.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.tml.myfruitdairy.data.converter.FruitConverter;

import java.util.ArrayList;

@Entity
public class SaveFruitModel {
    @PrimaryKey
    @NonNull
    public int id;

    @ColumnInfo(name = "date")
    public String date;

    @ColumnInfo(name = "fruits")
    @TypeConverters(FruitConverter.class)
    public ArrayList<FruitModel> fruit;


    public String totalVitamins;
    public String totalAmount;
}
