package com.tml.myfruitdairy.data.dbInterface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;

import java.util.ArrayList;
import java.util.List;


@Dao
public interface FruitInterface {

    @Query("SELECT COUNT(id) FROM FruitModel")
    int dataCount();

    @Query("SELECT * FROM FruitModel")
    List<FruitModel> getAll();

    @Query("SELECT * FROM SaveFruitModel ORDER BY date DESC")
    List<SaveFruitModel> getSaveFruit();

    @Query("SELECT * FROM SaveFruitModel WHERE date == :date")
    SaveFruitModel getSaveFruitByDate(String date);

    @Query("SELECT * FROM FruitModel WHERE id == :id")
    FruitModel getFruit(int id);

    @Query("SELECT * FROM FruitModel")
    List<FruitModel> getFruits();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSaveFruit(List<SaveFruitModel> saveFruitModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFruit(List<FruitModel> FruitModel);

    @Delete
    void deleteSaveFruit(SaveFruitModel saveFruitModel);

    @Query("DELETE FROM SaveFruitModel")
    void deleteAllSave();
}
