package com.tml.myfruitdairy;

import android.app.Application;

import androidx.room.Room;

import com.tml.myfruitdairy.data.AppDatabase;
import com.tml.myfruitdairy.data.DatabaseHelper;
import com.tml.myfruitdairy.network.RestServiceHelper;


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RestServiceHelper.getInstance().init(getApplicationContext(), "https://fruitdiary.test.themobilelife.com");
        DatabaseHelper.init(this);
    }

}
