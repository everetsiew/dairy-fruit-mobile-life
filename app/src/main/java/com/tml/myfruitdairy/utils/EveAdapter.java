package com.tml.myfruitdairy.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class EveAdapter<T extends RecyclerView.ViewHolder, D> extends RecyclerView.Adapter {
    private Context context;
    private List<D> dataList = new ArrayList<>();


    public Context getContext() {
        return context;
    }

    public EveAdapter(Context context) {
        this.context = context;
    }

    public void addData(D data) {
        if (data == null)
            return;
        this.dataList.add(data);
        notifyDataSetChanged();
    }

    public void addDataList(List<D> data) {
        if (data == null)
            return;
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void setDataList(List<D> data) {
        if (data == null)
            return;
        this.dataList = new ArrayList<>(data);
        notifyDataSetChanged();
    }


    public D getData(int position) {
        if (position >= this.dataList.size())
            return null;
        return this.dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return getDataCount();
    }

    public int getDataCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        //noinspection unchecked
        onGetView((T) viewHolder, getData(position), position);
    }

    public abstract RecyclerView.ViewHolder onGetView(T viewHolder, D data, int position);


    @NonNull
    @Override
    public T onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(setViewId(viewType), parent, false);

        return createViewHolder(itemView, viewType);
    }

    public abstract int setViewId(int viewType);

    public abstract T createViewHolder(View view, int viewType);

    public void dispose() {
        context = null;
    }

}
