package com.tml.myfruitdairy.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {
    private static DateTimeUtils instance;
    private SimpleDateFormat displayTime;

    public static DateTimeUtils getInstance() {
        return instance = instance == null ? new DateTimeUtils() : instance;
    }

    private DateTimeUtils() {
        displayTime = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    }

    public String getDateString(Calendar calendar) {
        try {
            return displayTime.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getDateString(int year, int month, int dayOfMonth) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            return displayTime.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
