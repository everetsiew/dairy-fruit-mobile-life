package com.tml.myfruitdairy.network;


import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public abstract class ObserverListener<T> extends DisposableObserver<Response<T>> {

    @Override
    public void onNext(Response<T> result) {
        try {
            action(result.code(), result.body());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onComplete() {
        dispose();
    }

    public abstract void action(int status, T result);

}
