package com.tml.myfruitdairy.network;


import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;
import com.tml.myfruitdairy.network.model.AddDateRequest;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApiHelper {

    @GET("/api/fruit")
    Observable<Response<List<FruitModel>>> getAvailableFruit();

    @GET("/api/entries")
    Observable<Response<List<SaveFruitModel>>> getSaveFruit();

    @Headers({"Content-Type: application/json"})
    @POST("/api/entries")
    Observable<Response<String>> addSaveDate(
            @Body AddDateRequest request
    );

    @Headers({"Content-Type: application/json"})
    @POST("/api/entry/{entryId}/fruit/{fruitId}")
    Observable<Response<String>> editFruit(
            @Path("entryId") int entryId,
            @Path("fruitId") int fruitId,
            @Query("amount") int amount
    );

    @DELETE("/api/entry/{entryId}")
    Observable<Response<String>> deleteEntry(
            @Path("entryId") int entryId
    );

}
