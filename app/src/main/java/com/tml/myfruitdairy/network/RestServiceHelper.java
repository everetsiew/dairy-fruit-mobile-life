package com.tml.myfruitdairy.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RestServiceHelper {
    private String apiUrl;
    private static RestServiceHelper instance;
    private RestApiHelper restApiHelper;

    private final long CONNECT_TIMEOUT = 60L;
    private final long READ_TIMEOUT = 60L;
    private final long WRITE_TIMEOUT = 60L;

    private ConnectivityManager connectivityManager;

    private RestServiceHelper() {
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public long getREAD_TIMEOUT() {
        return READ_TIMEOUT;
    }

    public void init(Context context, String apiUrl) {
        this.apiUrl = apiUrl;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        CookieHandler cookieHandler = new CookieManager();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(logging)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .retryOnConnectionFailure(false)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor((Interceptor.Chain chain) -> {

                    Request.Builder builder = chain.request().newBuilder();

                    return chain.proceed(builder.build());
                })
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        restApiHelper = retrofit.create(RestApiHelper.class);
    }

    private boolean isConnected() {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public RestApiHelper getRestApiHelper() {
        return restApiHelper;
    }

    public static RestServiceHelper getInstance() {
        return instance = instance == null ? new RestServiceHelper() : instance;
    }

    public static void dispose() {
        instance = null;
    }

    public static RestApiHelper call() {
        return getInstance().restApiHelper;
    }

    public static RestServiceHelper observe(Observable<?> observable, ObserverListener observerListener) {
        if (instance == null)
            instance = new RestServiceHelper();
        try {
            if (observerListener == null)
                return instance;
            if (!instance.isConnected()) {
                observerListener.action(1000, null);
                return instance;
            }

            observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observerListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }


}
