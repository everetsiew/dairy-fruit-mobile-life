package com.tml.myfruitdairy.network.model;

public class AddDateRequest {
    public String date;

    public AddDateRequest(String date) {
        this.date = date;
    }
}
