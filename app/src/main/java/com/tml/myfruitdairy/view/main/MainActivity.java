package com.tml.myfruitdairy.view.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.tml.myfruitdairy.R;
import com.tml.myfruitdairy.view.main.about.AboutFragment;
import com.tml.myfruitdairy.view.main.fruit.FruitFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment("My Dairy", FruitFragment.newInstance());
        adapter.addFragment("About", AboutFragment.newInstance());
        pager.setAdapter(adapter);
    }

    public static class PagerAdapter extends FragmentPagerAdapter {
        private final ArrayList<String> fragmentTitle = new ArrayList<>();
        private final ArrayList<Fragment> fragmentList = new ArrayList<>();

        public PagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(String name, Fragment fragment) {
            fragmentTitle.add(name);
            fragmentList.add(fragment);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            if (position < fragmentList.size())
                return fragmentList.get(position);
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }

    }


}