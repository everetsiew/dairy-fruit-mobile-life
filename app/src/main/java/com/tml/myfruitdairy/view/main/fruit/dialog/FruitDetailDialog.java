package com.tml.myfruitdairy.view.main.fruit.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tml.myfruitdairy.R;
import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;

public class FruitDetailDialog extends Dialog {
    private SaveFruitModel data;


    public FruitDetailDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public FruitDetailDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    private void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.view_dialog_fruit_detail);
        invalidateData();
    }

    public FruitDetailDialog setData(SaveFruitModel saveFruitModel) {
        this.data = saveFruitModel;
        invalidateData();
        return this;
    }

    private void invalidateData() {
        if (data != null) {
            ((TextView) findViewById(R.id.title)).setText(data.date);
            String descriptionString = "";
            for (FruitModel fruit : data.fruit)
                descriptionString += fruit.fruitType.toUpperCase() + " x" + fruit.amount + " = " + fruit.vitamins + " vitamins\n";

            if (descriptionString.length() <= 0)
                descriptionString = "No Fruits";

            ((TextView) findViewById(R.id.description)).setText(descriptionString);
        }
    }
}
