package com.tml.myfruitdairy.view.main.fruit.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.tml.myfruitdairy.R;
import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.network.RestServiceHelper;

import java.util.List;

public class FruitEditDialog extends Dialog {
    private List<FruitModel> fruits;
    private ArrayAdapter adapter;
    private int fruitCount = 1;
    private int selectedFruit;
    private FruitEditDialogListener listener;

    public interface FruitEditDialogListener {
        void onConfirm(FruitModel fruitModel, int count);
    }

    public FruitEditDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public FruitEditDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    public FruitEditDialog setListener(FruitEditDialogListener listener) {
        this.listener = listener;
        return this;
    }

    private void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.view_dialog_edit_fruit);

        Spinner spin = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFruit = position;
                Glide.with(getContext()).load(RestServiceHelper.getInstance().getApiUrl() + "/" + fruits.get(position).image).into((ImageView) findViewById(R.id.fruitImg));

                ((TextView) findViewById(R.id.countText)).setText(String.valueOf(fruitCount));
                ((TextView) findViewById(R.id.vitaminCount)).setText((fruitCount * fruits.get(selectedFruit).vitamins + " vitamins"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        findViewById(R.id.minus).setOnClickListener((view) -> {
            fruitCount--;
            if (fruitCount < 0)
                fruitCount = 0;
            ((TextView) findViewById(R.id.countText)).setText(String.valueOf(fruitCount));
            ((TextView) findViewById(R.id.vitaminCount)).setText((fruitCount * fruits.get(selectedFruit).vitamins + " vitamins"));
        });
        findViewById(R.id.add).setOnClickListener((view) -> {
            fruitCount++;
            ((TextView) findViewById(R.id.countText)).setText(String.valueOf(fruitCount));
            ((TextView) findViewById(R.id.vitaminCount)).setText((fruitCount * fruits.get(selectedFruit).vitamins + " vitamins"));
        });
        findViewById(R.id.confirm).setOnClickListener((view) -> {
            if (listener != null)
                listener.onConfirm(fruits.get(selectedFruit), fruitCount);
            dismiss();
        });
    }

    public FruitEditDialog setData(List<FruitModel> fruits) {
        this.fruits = fruits;
        String[] fruitString = new String[fruits.size()];
        for (int i = 0; i < fruits.size(); i++)
            fruitString[i] = fruits.get(i).type.toUpperCase();
        adapter.addAll(fruitString);
        return this;
    }

}
