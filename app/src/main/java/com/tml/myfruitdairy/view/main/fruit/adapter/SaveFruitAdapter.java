package com.tml.myfruitdairy.view.main.fruit.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tml.myfruitdairy.R;
import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;
import com.tml.myfruitdairy.utils.EveAdapter;

public class SaveFruitAdapter extends EveAdapter<SaveFruitAdapter.ViewHolder, SaveFruitModel> {
    private SaveFruitAdapterListener listener;

    public interface SaveFruitAdapterListener {
        void onItemClick(int viewId, int position, SaveFruitModel data);

    }

    public SaveFruitAdapter(Context context) {
        super(context);
    }

    public SaveFruitAdapter setListener(SaveFruitAdapterListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public RecyclerView.ViewHolder onGetView(ViewHolder viewHolder, SaveFruitModel data, int position) {
        viewHolder.itemView.setTag(position);
        viewHolder.edit.setTag(position);
        viewHolder.delete.setTag(position);

        viewHolder.fruitCount.setText(((data.fruit.size() == 0 ? "No" : data.fruit.size()) + " Fruits Type"));

        if (data.totalVitamins == null) {
            int vitamins = 0;
            int amount = 0;
            for (FruitModel fruit : data.fruit) {
                vitamins += fruit.vitamins;
                amount += fruit.amount;
            }
            data.totalVitamins = String.valueOf(vitamins);
            data.totalAmount = String.valueOf(amount);
        }
        viewHolder.vitaminCount.setText(("total "+data.totalAmount + " fruits with " + data.totalVitamins + " vitamins."));
        viewHolder.dateTime.setText(data.date);

        return viewHolder;
    }

    @Override
    public int setViewId(int viewType) {
        return R.layout.view_list_save_fruit;
    }

    @Override
    public ViewHolder createViewHolder(View view, int viewType) {
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener((View v) -> {
            if (listener != null && v.getTag() instanceof Integer) {
                int position = (int) v.getTag();
                listener.onItemClick(-1, position, getData(position));
            }
        });
        viewHolder.edit.setOnClickListener((View v) -> {
            if (listener != null && v.getTag() instanceof Integer) {
                int position = (int) v.getTag();
                listener.onItemClick(v.getId(), position, getData(position));
            }
        });
        viewHolder.delete.setOnClickListener((View v) -> {
            if (listener != null && v.getTag() instanceof Integer) {
                int position = (int) v.getTag();
                listener.onItemClick(v.getId(), position, getData(position));
            }
        });
        return viewHolder;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView fruitCount, vitaminCount, dateTime;
        public View edit, delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fruitCount = itemView.findViewById(R.id.fruitCount);
            vitaminCount = itemView.findViewById(R.id.vitaminCount);
            dateTime = itemView.findViewById(R.id.dateTime);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
