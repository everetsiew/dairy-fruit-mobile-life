package com.tml.myfruitdairy.view.main.fruit;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tml.myfruitdairy.R;
import com.tml.myfruitdairy.data.DatabaseHelper;
import com.tml.myfruitdairy.data.model.FruitModel;
import com.tml.myfruitdairy.data.model.SaveFruitModel;
import com.tml.myfruitdairy.network.ObserverListener;
import com.tml.myfruitdairy.network.RestServiceHelper;
import com.tml.myfruitdairy.network.model.AddDateRequest;
import com.tml.myfruitdairy.utils.DateTimeUtils;
import com.tml.myfruitdairy.view.main.fruit.adapter.SaveFruitAdapter;
import com.tml.myfruitdairy.view.main.fruit.dialog.FruitDetailDialog;
import com.tml.myfruitdairy.view.main.fruit.dialog.FruitEditDialog;

import java.util.Calendar;
import java.util.List;

public class FruitFragment extends Fragment {
    private SaveFruitAdapter adapter;

    public static FruitFragment newInstance() {
        return new FruitFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fruit, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter = new SaveFruitAdapter(getContext())
                .setListener((int viewId, int position, SaveFruitModel data) -> {
                    if (viewId == -1) {
                        new FruitDetailDialog(getContext()).setData(data).show();
                    } else if (viewId == R.id.edit) {
                        AsyncTask.execute(() -> {
                            List<FruitModel> fruits = DatabaseHelper.getDatabase().chatRoomInterface().getFruits();
                            getActivity().runOnUiThread(() -> {
                                new FruitEditDialog(getContext()).setData(fruits)
                                        .setListener(((fruitModel, count) -> {
                                            //todo save fruit
                                            editFruit(data.id, fruitModel.id, count);
                                        })).show();
                            });
                        });
                    } else if (viewId == R.id.delete) {
                        deleteDate(data.id);
                    }
                }));

        loadSaveData();
        getAvailableFruit();

        view.findViewById(R.id.add).setOnClickListener((View v) -> {
            final Calendar calendar = Calendar.getInstance();
            new DatePickerDialog(getContext(), (dialogView, year, month, dayOfMonth) -> {
                AsyncTask.execute(() -> {
                    SaveFruitModel saveFruitModel = DatabaseHelper.getDatabase().chatRoomInterface().getSaveFruitByDate(DateTimeUtils.getInstance().getDateString(year, month, dayOfMonth));
                    if (saveFruitModel == null)
                        addDate(DateTimeUtils.getInstance().getDateString(year, month, dayOfMonth));
                    else
                        getActivity().runOnUiThread(() -> new FruitDetailDialog(getContext()).setData(saveFruitModel).show());
                });
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

        });


        return view;
    }


    private void getAvailableFruit() {
        RestServiceHelper.observe(RestServiceHelper.call().getAvailableFruit(),
                new ObserverListener<List<FruitModel>>() {
                    @Override
                    public void action(int status, List<FruitModel> result) {
                        if (result != null)
                            AsyncTask.execute(() -> {
                                DatabaseHelper.getDatabase().chatRoomInterface().insertFruit(result);
                                getSaveFruit();
                            });
                    }
                });
    }

    private void getSaveFruit() {
        RestServiceHelper.observe(RestServiceHelper.call().getSaveFruit(),
                new ObserverListener<List<SaveFruitModel>>() {
                    @Override
                    public void action(int status, List<SaveFruitModel> result) {
                        if (result != null)
                            AsyncTask.execute(() -> {
                                for (SaveFruitModel saveFruit : result)
                                    for (FruitModel fruit : saveFruit.fruit) {
                                        //massage data
                                        fruit.vitamins = DatabaseHelper.getDatabase().chatRoomInterface().getFruit(fruit.fruitId).vitamins * fruit.amount;
                                    }
                                DatabaseHelper.getDatabase().chatRoomInterface().deleteAllSave();
                                DatabaseHelper.getDatabase().chatRoomInterface().insertSaveFruit(result);
                                loadSaveData();
                            });
                    }
                });
    }

    private void editFruit(int entityId, int fruitId, int amount) {
        RestServiceHelper.observe(RestServiceHelper.call().editFruit(entityId, fruitId, amount),
                new ObserverListener<String>() {
                    @Override
                    public void action(int status, String result) {
                        getSaveFruit();
                    }
                });
    }

    private void loadSaveData() {
        AsyncTask.execute(() -> {
            List<SaveFruitModel> fruits = DatabaseHelper.getDatabase().chatRoomInterface().getSaveFruit();
            getActivity().runOnUiThread(() -> adapter.setDataList(fruits));
        });
    }

    private void addDate(String date) {
        RestServiceHelper.observe(RestServiceHelper.call().addSaveDate(new AddDateRequest(date)),
                new ObserverListener<String>() {
                    @Override
                    public void action(int status, String result) {
                        getSaveFruit();
                    }
                });
    }

    private void deleteDate(int entryId) {
        RestServiceHelper.observe(RestServiceHelper.call().deleteEntry(entryId),
                new ObserverListener<String>() {
                    @Override
                    public void action(int status, String result) {
                        getSaveFruit();
                    }
                });
    }

}
